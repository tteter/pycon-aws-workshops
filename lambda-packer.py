import argparse
import os
import shutil
import subprocess
import sys
from tempfile import gettempdir

import yaml


class LambdaPacker:

    def __init__(self, config_path: str):
        self._config = self._parse_config(path=config_path)
        self._temp_dir = os.path.join(gettempdir(), 'lambda_packer_tmp')

    def build_deployment_package(self):
        for source_path, destination_path in self._config.items():
            if not os.path.isdir(source_path):
                raise Exception(f"No such directory: {source_path}")
            self._cleanup_temp_dir()
            shutil.copytree(src=source_path, dst=self._temp_dir, ignore=shutil.ignore_patterns('.*'))
            self._install_requirements(self._temp_dir)
            self._create_zip_archive(destination_path)
            self._cleanup_temp_dir()

    def _parse_config(self, path: str) -> dict:
        with open(path, 'r') as config_file:
            return dict(yaml.safe_load(config_file))

    def _install_requirements(self, target_dir: str) -> None:
        if not os.path.isdir(target_dir):
            raise Exception(f"No such directory: {target_dir}")
        requirements_path = os.path.join(target_dir, 'requirements.txt')
        if os.path.isfile(requirements_path):
            subprocess.call([sys.executable, '-m', 'pip', 'install', '-r', requirements_path, '-t', target_dir])

    def _create_zip_archive(self, destination_path: str) -> None:
        if destination_path.endswith('.zip'):
            destination_path = destination_path.strip('.zip')
        shutil.make_archive(destination_path, 'zip', self._temp_dir)

    def _cleanup_temp_dir(self) -> None:
        if os.path.isdir(self._temp_dir):
            shutil.rmtree(self._temp_dir)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, type=argparse.FileType('r'),
                        help='YAML config file with packages to build.')
    args = parser.parse_args()
    LambdaPacker(config_path=args.config.name).build_deployment_package()
