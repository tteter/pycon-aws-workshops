import decimal
import json
import logging
import os
import boto3 as boto3
from boto3.dynamodb.conditions import Key

logger = logging.getLogger()
logger.setLevel("DEBUG")


def get_calls_statistics(event: dict, context: dict = None) -> dict:
    table_name = os.getenv('dynamoDB_table')
    query_params = event.get("queryStringParameters") or {}
    title = query_params.get("title")
    dynamoDB = boto3.resource('dynamodb', region_name='eu-central-1')
    crashes_table = dynamoDB.Table(table_name)
    if not title:
        response = crashes_table.scan()
    else:
        response = crashes_table.scan(FilterExpression=Key('title').eq(title))
    if response.get('Items'):
        logger.debug(f"Items found: {response['Items']}")
    else:
        logger.debug(f"No items has been found in table: {table_name}")
        return _format_response(404, {"message": f"No items has been found in table: {table_name}"})
    return _format_response(200, response['Items'])


def _format_response(status_code: int, data: dict) -> dict:
    return {
        'statusCode': status_code,
        'body': json.dumps(data, cls=DecimalEncoder)
    }


HANDLERS = {
    ('GET', '/statistics'): get_calls_statistics,
}


def lambda_handler(event: dict, context: dict = None) -> dict:
    resource_method = event['httpMethod'].upper(), event['resource']
    if resource_method in HANDLERS:
        return HANDLERS[resource_method](event, context)
    else:
        return {
            'statusCode': 405
        }


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            if o % 1 > 0:
                return float(o)
            else:
                return int(o)
        return super(DecimalEncoder, self).default(o)
