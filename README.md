# Introduction

## Links

[Accounts](https://docs.google.com/spreadsheets/d/1K3T_IGBGFYwZJU8R5jDf-1IziVhIbYeE-70fGCTOiS0/edit?usp=sharing)

[GitLab repository](https://gitlab.com/tteter/pycon-aws-workshops)

[AWS Console login](https://signin.aws.amazon.com/console)

## Fill in login form with the information below

- account ID (AWS root account id) : `gu-lab`
- IAM user name : `xxx`
- Password: `xxx`

## AWS Region change

Right after successful login to your account, change AWS region where your resources will be created. In order to do it, select EU (Frankfurt) region from drop down menu in the upper right corner (next to the button with your username)

![alt text](https://gitlab.com/tteter/pycon-aws-workshops/raw/master/region.png "Change region")

## AWS Lambda creation

- in services menu `search for Lambda`
- click on `Create a function`
- create new lambda function `from scratch`
- enter function name according to pattern: `pycon-<your_username_here>-test-function`
- choose `Python as a runtime`
- expand execution role section
- select `“use existing role”`
- select role `“service-role/PyConLambdaRole”`
- click `create function`

## API Gateway

- in services menu `search for API Gateway`
- click `“create API”`
- enter `API name`
- leave default configuration
- click `“create API”`

## API Gateway - create new GET method

- click on `resources tab`
- select `root resource (“/”)`
- choose `“create method”` from `Actions` dropdown
- select `GET method type` and `confirm`
- click on square presenting created method
- select `eu-central-1` for lambda region
- enter lambda function name (the one you previously created)
- `save changes` and `confirm lambda permissions popup` that will come up

## Deploy API

- click on `resources tab`
- select `root resource (“/”)`
- choose `“deploy API”` from `Actions` dropdown
- select `“NEW STAGE”` as deployment stage
- enter stage name according to scheme: `pycon-<your_username_here>-stage`
- click `deploy API`
- `Ignore permission warning` and click on `invoke URL` that was generated 

![alt text](https://gitlab.com/tteter/pycon-aws-workshops/raw/master/url.png "Click on invoke URL")

After clicking on provided invoke URL you should see output as on the screenshot below:

![alt text](https://gitlab.com/tteter/pycon-aws-workshops/raw/master/output.png "HelloWorld endpoint response")

__Note: you need to redeploy API whenever it changes. It does not have to be redeployed when changes affects only Lambda functions.__

## Cloud Watch

- in services menu search for `CloudWatch`
- go to `Logs tab`
- Logs from executing your function are available under `/aws/lambda/<your_function_name_here>` log group

There is plenty of useful information, such as execution time and memory usage. 
To log anything from your lambda function, you can use both print and logging module.

## Boto3

To use Boto3 locally from your interpreter you have to setup credentials file:

- create file `~/.aws/credentials`
- paste there your `access key` and `secret access key` in format as shown below

```bash
[default]
aws_access_key_id=ASIAXXXXXXXXXXXXXXX
aws_secret_access_key=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

- create file `~/.aws/config` with the following content:

```bash
[default]
region=eu-central-1
```

# Exercises

## 1. Get emergency statistics

Implement lambda function that is triggered by GET request on `/statistics` endpoint in API Gateway. Lambda shall scan `pycon-emergencies-<your_number_zero_padded>` dynamoDB table for it’s items using `boto3` (Python SDK for AWS) and return them to the user in response.
Response has to consist of statusCode and json formatted body (so it can be converted to HTTP response and returned via API Gateway).

*** It should be possible to filter items by `“title”` field (user should be able to provide title as `query parameter to GET method request`).

### Documentation

https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.04.html

### DynamoDB

https://eu-central-1.console.aws.amazon.com/dynamodb/home?region=eu-central-1#tables:selected=pycon-emergencies;tab=items

### Expected results

You can compare format of your results with output from the url:
https://i795v6a1sj.execute-api.eu-central-1.amazonaws.com/dev/statistics

https://i795v6a1sj.execute-api.eu-central-1.amazonaws.com/dev/statistics?title=MURDER

## 2. Post new emergency

Implement lambda function that is triggered by POST request on `/report-emergency` endpoint in API Gateway. Lambda gets an event argument with body of type application/json which is equivalent to an item that needs to be inserted into `pycon-emergencies-<your_number>` dynamoDB table. Lambda should return response with all information (including statusCode) gained from boto3 client put method. Exceptions that may occur when putting new item to DB need to be handled by logging to CloudWatch and returning proper response.

### Documentation

https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.03.html

### DynamoDB

https://eu-central-1.console.aws.amazon.com/dynamodb/home?region=eu-central-1#tables:selected=pycon-emergencies;tab=items

### Test body

```json
{
    "addr": "Elm Street",
    "id": <some unique integer>,
    "lat": 40.1000,
    "lng": -75.0101,
    "timeStamp": "2015-12-10 17:10:52",
    "title": "MURDER",
    "twp": "NEW HANOVER",
    "zip": 19525
}
```

### Expected results

- new item is added to dynamoDB table
- exceptions are handled and logged to CloudWatch
- response body should include information about DB update

## 3. Call police or ambulance whenever new emergency occurs

Implement lambda function that is triggered by an update on `pycon-emergencies-<your_number>` dynamoDB table. Lambda shall check on all newly added records and invoke `pycon-emergency-dispatcher` lambda function that will call relevant services and log information about new emergencies to CloudWatch. All the information about the emergency should be forwarded as payload.

### Documentation

https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/lambda.html

### Expected results

- Lambda is triggered by DynamoDB streams (one dynamoDB update == one lambda execution)
- pycon-emergency-dispatcher lambda should be invoked whenever new emergency occurs (check in CloudWatch if it did)
- all invocation information, such as: payload, invocationType, function name etc. should be logged to CloudWatch

# Materials

[Serverless documentation](https://serverless.com/framework/docs/)

[Build your first Serverless Web Application](https://aws.amazon.com/serverless/build-a-web-app/)

[Boto3 documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)

[AWS cost calculator](https://servers.lol)

[Terraform documentation](https://www.terraform.io/docs/index.html)

[10 AWS Lambda Use Cases to Start Your Serverless Journey](https://www.simform.com/serverless-examples-aws-lambda-use-cases/)

[Quiz](https://quizizz.com/join)

[Feedback survey](https://www.surveymonkey.com/r/G6X5589)
