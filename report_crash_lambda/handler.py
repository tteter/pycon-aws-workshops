import decimal
import json
import os

import boto3


def report_emergency(event: dict, context: dict = None) -> dict:
    try:
        table_name = os.getenv('dynamoDB_table')
        dynamoDB = boto3.resource('dynamodb', region_name='eu-central-1')
        crashes_table = dynamoDB.Table(table_name)
        item_data = json.loads(event.get('body', {}), parse_float=decimal.Decimal)
        if not item_data:
            return _format_response(500, {"exception": 'No data in request body!'})

        response = crashes_table.put_item(Item=item_data)
        print(response)
    except Exception as exc:
        return _format_response(500, {"exception": str(exc)})

    return _format_response(response['ResponseMetadata']['HTTPStatusCode'], response)


def _format_response(status_code: int, data: dict) -> dict:
    return {
        'statusCode': status_code,
        'body': json.dumps(data)
    }


HANDLERS = {
    ('POST', '/report'): report_emergency,
}


def lambda_handler(event: dict, context: dict = None) -> dict:
    resource_method = event['httpMethod'].upper(), event['resource']
    if resource_method in HANDLERS:
        return HANDLERS[resource_method](event, context)
    else:
        return {
            'statusCode': 405
        }
