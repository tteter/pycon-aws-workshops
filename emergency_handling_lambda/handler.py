import json
import logging
import os

import boto3

logger = logging.getLogger()
logger.setLevel("DEBUG")


def handle_emergency(event: dict, context: dict = None) -> dict:
    dispatcher_function_name = os.getenv('DISPATCHER_FUNCTION')
    invocation_type = 'Event'
    try:
        for record in event['Records']:
            if record['eventName'] == 'INSERT':
                record['username'] = 'TOMEKK'
                aws_lambda = boto3.client('lambda')
                logger.debug(f'Calling function: {dispatcher_function_name}. '
                             f'Invocation type: {invocation_type} \nPayload: {json.dumps(record)}')

                aws_lambda.invoke(FunctionName=dispatcher_function_name,
                                  InvocationType=invocation_type,
                                  Payload=json.dumps(record))
    except Exception as exc:
        logger.error(f'Exception: {exc}')
        raise

    return {'statusCode': 200}


def _format_response(status_code: int, data: dict) -> dict:
    return {
        'statusCode': status_code,
        'body': json.dumps(data)
    }
