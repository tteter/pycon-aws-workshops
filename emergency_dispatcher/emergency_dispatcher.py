import json
import logging

logger = logging.getLogger()
logger.setLevel("DEBUG")


def lambda_handler(event, context):
    # TODO implement
    logger.debug(f'Event: {event}')
    try:
        username = event['username']
        event_type = event['eventName']
        if event_type != 'INSERT':
            raise Exception("It is not a new emergency! (eventName should be INSERT)")

        item_data = event['dynamodb']['NewImage']
        address = item_data['addr']['S']

        logger.info(f'User {username} has invoked the Dispatcher.')
        logger.info(f'Emergency services were called. They are on their way to: {address}')
    except Exception as exc:
        logger.error(f"Exception: {exc}")
        raise
    return {
        'statusCode': 200,
        'body': json.dumps('World have been saved!')
    }
